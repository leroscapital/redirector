﻿using System;
using System.Linq;
using Xunit;
using Xunit.Abstractions;
using ReDirector;

namespace UnitTest
{
    using NLog;

    public class UnitTest1
    {
        private static Logger log = LogManager.GetCurrentClassLogger();

        private readonly ITestOutputHelper output;

        public UnitTest1(ITestOutputHelper output)
        {
            this.output = output;
        }

        [Fact]
        public void Test_Null_Converted_to_Int_equals_zero()
        {
            int expected = 0;
            int actual = Convert.ToInt32(null);
            Assert.True(expected == actual);
        }

        [Fact]
        public void Test_Timespan_Compare()
        {
            if (DateTime.Now.TimeOfDay.Hours < 17)
            {
                Assert.True(DateTime.Now.TimeOfDay < TimeSpan.FromHours(17.01667));
            }
            double time = DateTime.Now.TimeOfDay.Add(TimeSpan.FromMinutes(1)).TotalHours;
            output.WriteLine("time: {0} inHours: {1}", DateTime.Now.TimeOfDay, time);
            Assert.True(DateTime.Now.TimeOfDay < TimeSpan.FromHours(time));
        }

        [Theory]
        [InlineData(2017, 11, 28)]
        public void Test_datetime_compare(int yr, int mo, int d)
        {
            DateTime dt1 = new DateTime(9999, 1, 1);
            DateTime dt2 = new DateTime(yr, mo, d);
            // less than means dt1 is earlier
            Assert.True(dt1 > dt2);
            Assert.False(dt2 > DateTime.Today);
            //output.WriteLine(DateTime.Today.ToLongDateString()); full month, day, year
            //output.WriteLine(DateTime.Today.ToLongTimeString()); 12:00:00 AM
            //output.WriteLine(DateTime.Now.ToString()); mm/dd/yy hh:mm:ss am/pm
        }

        [Fact]
        public void Test_datetime_exp_preclose()
        {
            DateTime exp = DateTime.Today;
            DateTime product_details = DateTime.MaxValue;
            Assert.True(product_details > exp);



        }

        [Theory]
        [InlineData("password")]
        [InlineData("12345678")]
        [InlineData("W3lc0me$$!")]
        [InlineData("")]
        [InlineData("0")]
        [InlineData("A")]
        public void Test_base64_encode_decode(string s)
        {
            output.WriteLine("encode: {0}", Utility.Base64Encode(s));

            Assert.Equal(s, Utility.Base64Decode(Utility.Base64Encode(s)));
        }

        [Fact]
        public void Test_xml_read_write()
        {
            string filename = @"C:\Users\Administrator\source\repos\redirector\ReDirector\bin\Debug\config\Config.xml";
            Assert.True(System.IO.File.Exists(filename));

        }

        [Fact]
        public void Test_ImportTask()
        {
            bool result = true;
            try
            {
                Utility.ImportXMLTask("ReDirector", new TimeSpan(DateTime.Now.Hour, DateTime.Now.Minute + 2, DateTime.Now.Second));
            }
            catch (Exception ex)
            {
                log.Debug(ex);
                result = false;
            }

            Assert.True(result);
        }

        [Fact]
        public void Test_Simpletask()
        {
            Utility.CreateSimpleTask();
            Assert.True(true);
        }

        [Fact]
        public void Test_DeleteTask()
        {
            Utility.GetAndDeleteTask("ReDirector");
            Assert.True(true);
        }

        [Fact]
        public void Test_ChangeTask()
        {
            Utility.GetAndChangeTask("ReDirector", new TimeSpan(DateTime.Now.Hour, DateTime.Now.Minute + 2, DateTime.Now.Second));
            Assert.True(true);
        }

        [Fact]
        public void Test_Username()
        {
            string one = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
            string two = Environment.UserDomainName + "\\" + Environment.UserName;
            Assert.Equal(one, two);
            log.Debug(System.Security.Principal.WindowsIdentity.GetCurrent().User);
            log.Debug(System.Security.Principal.WindowsIdentity.GetCurrent().Token);
        }
    }
}
