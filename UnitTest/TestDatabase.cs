﻿using System;
using System.Configuration;
using ReDirector;
using Xunit;
using Xunit.Abstractions;

namespace UnitTest 
{
    using Npgsql;
    using System.Data;

    public class TestDatabase
    {
        private readonly ITestOutputHelper output;
        private DBConnect dbc;
        private NpgsqlConnection db_conn;

        public TestDatabase(ITestOutputHelper output)
        {
            this.output = output;
            this.dbc = new DBConnect();
            this.db_conn = this.dbc.GetConnection();
        }

        [Fact]
        public void TestCreatePostgresDB()
        {
            string testName = "redirector";
            dbc.Create(testName);
            
            Assert.True(dbc.IfDBExists(testName));
        }

        [Fact]
        public void Test_clean_string()
        {
            string test_string = "  exchange ticker  ";
            string expected = "exchange_ticker";

            string actual = string.Join("_", test_string.Split(default(string[]), StringSplitOptions.RemoveEmptyEntries));

            Assert.Equal(expected, actual);

        }

        [Fact]
        public void Test_hash_password()
        {
            byte[] salt;
            new System.Security.Cryptography.RNGCryptoServiceProvider().GetBytes(salt = new byte[16]);

            output.WriteLine(salt.ToString());
            Assert.NotNull(salt);
        }

        [Fact]
        public void Test_execute_scalar()
        {
            int ticker_id = 266;
            DateTime expiry = new DateTime(2017,12,28);

            NpgsqlCommand cmd = new NpgsqlCommand("SELECT id FROM twc.contracts WHERE ticker_id=@tick AND expiry=@exp;");
            cmd.Parameters.AddWithValue("tick", ticker_id);
            cmd.Parameters.AddWithValue("exp", expiry);
            cmd.Connection = dbc.GetConnection();

            object result = cmd.ExecuteScalar();
            output.WriteLine(result.ToString());

            Assert.True(Convert.ToInt32(result) == 4);

            DateTime expiry2 = new DateTime(2018, 5, 15);

            NpgsqlCommand cmd2 = new NpgsqlCommand("SELECT id FROM twc.contracts WHERE ticker_id=@tick AND expiry=@exp;");
            cmd2.Parameters.AddWithValue("tick", ticker_id);
            cmd2.Parameters.AddWithValue("exp", expiry2);
            cmd2.Connection = dbc.GetConnection();

            object result2 = cmd2.ExecuteScalar();
            output.WriteLine(Convert.ToString(result2));
            // if no result from query ExecuteScalar returns null
            Assert.Null(result2);
        }

        [Fact]
        public void Test_getContractID()
        {
            if (db_conn.State.Equals(ConnectionState.Open)) {
                NpgsqlCommand cmd = new NpgsqlCommand
                {
                    CommandText = "select max(id) from contracts;",
                    Connection = this.db_conn
                };
                object result = cmd.ExecuteScalar();
                Assert.IsType<Int32>(result);
                output.WriteLine("Max id: {0}", result);
                int maxID = Convert.ToInt32(result);

                cmd.CommandText = "INSERT INTO contracts (ticker_id, month, expiry) VALUES (@ticker_id, @contract_month, @expiry) ON CONFLICT DO NOTHING RETURNING id;";
                cmd.Parameters.AddWithValue("ticker_id", (new Random()).Next(1, 999));
                cmd.Parameters.AddWithValue("contract_month", new DateTime(2100, 1, 23));
                cmd.Parameters.AddWithValue("expiry", new DateTime(2100, (new Random()).Next(1,13), (new Random()).Next(1,29)));
                result = cmd.ExecuteScalar();
                output.WriteLine("Returned: {0}", result);
                Assert.True(Convert.ToInt32(result) == (maxID + 1));

                // returns null if already in database otherwise id
                result = cmd.ExecuteScalar();
                Assert.Null(result);

            }
        }

        void Dispose()
        {
            dbc.Close();
        }
    }
}
