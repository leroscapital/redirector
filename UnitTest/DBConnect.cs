﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTest 
{
    using Npgsql;
    // http://www.npgsql.org/doc/index.html

    public class DBConnect
    {
        private static NpgsqlConnection conn;

        public DBConnect()
        {
            string connStr = "Host=192.168.1.2;Username=postgres;Password=password;Database=redirector;Port=5432";
            conn = new NpgsqlConnection(connStr);
            conn.Open();
        }

        public NpgsqlConnection GetConnection()
        {
            return conn;
        }

        public void Close()
        {
            conn.Close();
        }
            

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities")]
        public bool IfDBExists(string dbname)
        {
            NpgsqlCommand find_database = new NpgsqlCommand(String.Format(
                "SELECT count(*) FROM pg_catalog.pg_database WHERE datname = '{0}';", dbname),
                conn);
            try
            {
                conn.Open();
                object result = find_database.ExecuteScalar();
                // type is System.Int64

                return Convert.ToBoolean(result);
            }
#pragma warning disable CS0168 // Variable is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // Variable is declared but never used
            {
            }
            finally
            {
                conn.Close();
            }
            return false; 

        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities")]
        public void Create(string db_name)
        {
            // https://stackoverflow.com/questions/17838913/postgresql-create-database-table-dynamically

            NpgsqlCommand m_createdb_cmd = new NpgsqlCommand(String.Format(@"
                CREATE DATABASE {0}
                WITH OWNER = postgres
                ENCODING = 'UTF8'
                CONNECTION LIMIT = -1;
                ",db_name),
                conn);

            
            try
            {
                conn.Open();
                if (!IfDBExists(db_name))
                {
                    m_createdb_cmd.ExecuteNonQuery();
                }
            }
#pragma warning disable CS0168 // Variable is declared but never used
            catch (Exception e)
#pragma warning restore CS0168 // Variable is declared but never used
            {
            }
            finally
            {
                conn.Close();
            }
        }
        
    }
}
