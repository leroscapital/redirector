﻿using System;
using System.Windows.Forms;
using System.Threading;

namespace ReDirector
{
    using NLog;

    partial class FormMainUI : Form
    {
        static Logger log = LogManager.GetCurrentClassLogger();
        TTAPIFunctions ttapiThread;

        public delegate void UpdateStatus(string label);  // delegate type
        public UpdateStatus UpdateMainUIMessage;              // delegate object

        public delegate void UpdateInsertLabel(string label);
        public UpdateInsertLabel GetInserts;

        public delegate void UpdateCounts(int f, int s);
        public UpdateCounts GetCounts;

        public delegate void DailyShutdown(bool b);
        public DailyShutdown GetStartShutdown;

        public FormMainUI()
        {
            InitializeComponent();
            UpdateMainUIMessage = new UpdateStatus(UpdateUIStatus);
            GetInserts = new UpdateInsertLabel(UpdateToolbarStatus);
            GetCounts = new UpdateCounts(UpdateContractCounts);
            GetStartShutdown = new DailyShutdown(InitiateShutdown);

        }

        private void UpdateContractCounts(int f, int s)
        {
            lblFuturesCount.Text = f.ToString();
            lblSpreadsCount.Text = s.ToString(); 
        }

        private void UpdateToolbarStatus(string message)
        {
            toolStripStatusLabel3.Text = message;
        }

        private void UpdateUIStatus(string message)
        {
            listBox1.Items.Insert(0, message);
        }

        private void InitiateShutdown(bool close)
        {
            if (this.ttapiThread != null)
            {
                this.ttapiThread.Dispose();
            }
            if (close)
            {
                this.Close();
            }
        }

        internal void ClearMessages()
        {
            listBox1.Items.Clear();
        }

        private void FormMainUI_Load(object sender, EventArgs e)
        {
            Configuration.TestConfiguration();

            if (ProgramOptionParser.DoAutostart())
            { startTTAPI(); }
        }
        
        private void FormMainUI_FormClosing(object sender, FormClosingEventArgs e)
        {
            log.Info("TTAPI shutdown initiated manually. Shutdown X out of main form.");
            InitiateShutdown(false);
        }

        private void ShutdownButton_Click(object sender, EventArgs e)
        {
            UpdateMainUIMessage("TTAPI Shutting down...");
            log.Info("TTAPI shutdown initiated manually. Shutdown button click.");
            // TODO: (future refinement) must close form: Exception System.Reflection.TargetInvocationException in TTAPI if start pressed after shutdown
            InitiateShutdown(true);
        }

        private void EditButton_Click(object sender, EventArgs e)
        {
            using (FormUserSettings getSettings = new FormUserSettings())
            {
                getSettings.ShowDialog();
            }
        }

        private void StartButton_Click(object sender, EventArgs e)
        {
            this.startTTAPI(); 
        }

        void startTTAPI()
        {
            string message = String.Format("Starting up at: {0}", DateTime.Now);
            UpdateMainUIMessage(message);
            log.Info(message);
            if (Configuration.UseSchedule) UpdateMainUIMessage(String.Format("Automatic shutdown scheduled for {0}", Configuration.ShutDownTime));
            UpdateMainUIMessage("Waiting for TTAPI Connection...");
            this.toolStripStatusLabel3.Text = "Connecting...";
            this.buttonShutdown.Enabled = true;

            TTAPIFunctions tf = new TTAPIFunctions();
            this.ttapiThread = tf;

            Thread workerThread = new Thread(tf.Start)
            {
                Name = "TTAPI"
            };
            workerThread.Start();
        }

        // open log file
        private void Button4_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(Environment.CurrentDirectory + @"\logs\redirector.log");
        }

        internal void Enable_Startup(bool b)
        {
            if (b)
            {
                this.buttonStart.Enabled = true;
            }
            else
            {
                this.buttonStart.Enabled = false;
            }
        }
    }
}