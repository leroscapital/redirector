﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;

namespace ReDirector
{
    //using MySql.Data.MySqlClient;
    using Npgsql;
    using NLog;
    using TradingTechnologies.TTAPI;

    class DataModel : IDisposable
    {
        private static Logger log = LogManager.GetCurrentClassLogger();
        private static Logger FailedInsert = LogManager.GetLogger("sql");

        private NpgsqlConnection dbrw;
        private Queue<NpgsqlCommand> sqlcmdQ = new Queue<NpgsqlCommand>();

        public List<ProductDetails> ProductList = new List<ProductDetails>();
        public Dictionary<string, int> ContractIDList = new Dictionary<string, int>();

        #region "SQL"
        // http://www.npgsql.org/doc/index.html 

        // read-only db version
        private NpgsqlCommand SQL_GetProductList()
        {
            string sql = @"
                SELECT
                und.id as underlying_id,
                inst.instrument,
                ac.asset_class,
                sec.sector,
                subsec.sub_sector,
                und.market,
                mktplc.mktplace,
                exch.exchange,
                tck.id as ticker_id,
                tck.ticker,
                tck.ct_size,
                tck.tick_size,
                tck.tick_value,
                tck.ct_qty,
                tck.ccy,
                tck.rt_active
                FROM
                twc.tickers tck
                INNER JOIN
                twc.instruments inst ON inst.id = tck.instrument_id
                INNER JOIN
                twc.categories cat ON cat.id = tck.category_id
                INNER JOIN
                twc.underlyings und ON und.id = tck.underlying_id
                INNER JOIN
                twc.asset_classes ac ON ac.id = und.asset_class_id
                INNER JOIN
                twc.sectors sec ON sec.id = und.sector_id
                INNER JOIN
                twc.sub_sectors subsec ON subsec.id = und.sub_sector_id
                INNER JOIN
                twc.marketplaces mktplc ON mktplc.id = und.mktplace_id
                INNER JOIN
                twc.exchanges exch ON exch.id = und.exchange_id
                WHERE
                cat.id = 1
                AND
                data_src_id = 4
                AND
                rt_active = TRUE
                ORDER BY ac.asset_class ASC, sec.sector ASC, subsec.sub_sector ASC, und.market ASC;
                ";

            return new NpgsqlCommand(sql);
        }

        // table_name can not be parameterized.  Does not pos security threat because it is created within the appication with information from database only.
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities")]
        private NpgsqlCommand SQL_CreateTableFuture(string table_name)
        {
            NpgsqlCommand cmd = new NpgsqlCommand
            {
                Connection = this.dbrw,
                CommandText = String.Format(@"
                CREATE TABLE IF NOT EXISTS {0} (
                  time_stamp TIMESTAMP,
                  contract_id INTEGER,
                  relative_contract INTEGER,
                  price NUMERIC,
                  volume INTEGER
                );
                ", table_name)
            };

            return cmd;
        }
        
        // table_name can not be parameterized.  Does not pos security threat because it is created within the appication with information from database only.
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities")]
        private NpgsqlCommand SQL_CreateTableSpread(string table_name)
        {
            NpgsqlCommand cmd = new NpgsqlCommand
            {
                Connection = this.dbrw,
                CommandText = String.Format(@"
                CREATE TABLE IF NOT EXISTS {0} (
                  time_stamp TIMESTAMP,
                  contract_id_1 INTEGER,
                  contract_id_2 INTEGER,
                  relative_contract_1 INTEGER,
                  relative_contract_2 INTEGER,
                  price NUMERIC,
                  volume INTEGER
                );
                ", table_name)
            };
//            cmd.CommandText = String.Format("CREATE TABLE IF NOT EXISTS {0} ", table_name);
//            cmd.CommandText += "(id SERIAL, time_stamp TIMESTAMP, contract_id_1 INTEGER, contract_id_2 INTEGER, relative_contract_1 INTEGER, relative_contract_2 INTEGER, price NUMERIC, volume INTEGER);";
            return cmd;
        }

        #endregion

        // Constructor
        public DataModel()
        {
            dbrw = new NpgsqlConnection(Configuration.GetDBConnection());
            dbrw.StateChange += Db_StateChange;
            dbrw.Notification += Dbrw_Notification;
            dbrw.Notice += Dbrw_Notice;
            dbrw.Disposed += Dbrw_Disposed;

            OpenDB(dbrw);
            GetProductsFromRODB();
            VerifyTableNames();
        }

        // Database operations
        private void OpenDB(NpgsqlConnection db)
        {
            try
            {
                db.Open();
            }
            catch (Exception ex)
            {
                Configuration.CrossThreadUILbUpdate("ERROR: Can not open database connection.");
                log.Fatal("Connection Failed: {0}", db.ConnectionString);
                log.Fatal(ex.Message);
                log.Fatal(ex);
            }
        }

        private void GetProductsFromRODB()
        {
            System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
            sw.Start();

            try
            {
                NpgsqlConnection db = new NpgsqlConnection();
#if DEBUG
                db.ConnectionString = "Host=twc1.crztytpj22mt.us-east-1.rds.amazonaws.com;Username=rt_user;Password=W3lc0me$$!;Database=twc;Port=5432";
#else
                db.ConnectionString = Configuration.GetDBConnection("ro"); 
#endif
                OpenDB(db);
                NpgsqlCommand cmd = this.SQL_GetProductList();
                cmd.Connection = db;

                NpgsqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    // underlying_id, instrument, asset_class, sector, sub_sector, market, mktplace, exchange, ticker_id, ticker, tck.ct_size,tck.tick_size,tck.tick_value,tck.ct_qty,tck.ccy,tck.rt_active
                    // 0              1           2            3       4           5        6         7        8          9       10          11            12             13         14       15
                    string exchange = CleanTTExchange(rdr.GetString(6).Trim());
                    string ticker = rdr.GetString(9).Trim();
                    int underlying_id = rdr.GetInt32(0);
                    int ticker_id = rdr.GetInt32(8);
                    string filename = CleanFileName(rdr.GetString(6), rdr.GetString(9));
                    int contract_size = rdr.GetInt32(10); //tck.ct_size
                    double tick_currency_value = rdr.GetDouble(12); //tck.tick_value

                    ProductList.Add(new ProductDetails(exchange, ticker, underlying_id, ticker_id, tick_currency_value, contract_size, filename));
                }
                rdr.Close();
                db.Close();
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                log.Error(ex);
            }

            sw.Stop();
            log.Info("AWS DB connect time: {0}", sw.ElapsedMilliseconds);
        }

        private void VerifyTableNames()
        {
            this.ProductList.ForEach(product =>
            {
                product._futuresTableName = String.Format("realtime_{0}_{1}", product._name, ProductType.Future.Name.ToLower());
                ExecuteSQL_NonQuery(SQL_CreateTableFuture(product._futuresTableName));
                product._spreadTableName = String.Format("realtime_{0}_{1}", product._name, ProductType.Spread.Name.ToLower());
                ExecuteSQL_NonQuery(SQL_CreateTableSpread(product._spreadTableName));
            });
        }

        public void CloseDB()
        {
            try
            {
                dbrw.Close();
            }
            catch (Exception ex)
            {
                log.Warn(ex.Message);
                log.Warn(ex);
            }
        }

        internal int GetCreate_ContractID(int ticker_id, string month, DateTime expiry)
        {
            // connect to twc database (main data db)
            NpgsqlConnection db = new NpgsqlConnection(Configuration.GetDBConnection("ro"));
            OpenDB(db);
            DateTime contract_month = DateTime.ParseExact(month, "MMMyy", new System.Globalization.CultureInfo("en-US"));
            NpgsqlCommand cmd = new NpgsqlCommand("SELECT id FROM twc.contracts WHERE ticker_id=@ticker_id AND month=@contract_month;");
            cmd.Parameters.AddWithValue("ticker_id", ticker_id);
            cmd.Parameters.AddWithValue("contract_month", contract_month);
            cmd.Connection = db;
            object result = Execute_Scalar(cmd);

            if (result == null)
            {
                // insert into database
                cmd.CommandText = "INSERT INTO twc.contracts (ticker_id, month, expiry) VALUES (@ticker_id, @contract_month, @expiry) RETURNING id;";
                cmd.Parameters.AddWithValue("ticker_id", ticker_id);
                cmd.Parameters.AddWithValue("contract_month", contract_month);
                cmd.Parameters.AddWithValue("expiry", expiry);

                // return new id value for this contract
                result = Execute_Scalar(cmd);
            }
            db.Close();
            return Convert.ToInt32(result); 
        }

        private const string COLUMNS_FUTURES = "(time_stamp,contract_id, relative_contract, price, volume)";
        // string variable pd_futuresTableName is not from user so there is no security risk.  
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities")]
        internal void InsertFuturesData(ReadOnlyCollection<TimeAndSalesData> data, Instrument i, ProductDetails pd)
        {
            NpgsqlCommand cmd = new NpgsqlCommand
            {
                Connection = this.dbrw,
                CommandText = String.Format("INSERT INTO {0} {1} VALUES (@timestamp,@contract_id,@relative,@price,@qty);", pd._futuresTableName, COLUMNS_FUTURES)
            };
            // foreach because data may have multiple entries
            foreach (TimeAndSalesData tsData in data)
            {
                log.Debug("{0}: {1} {2}",tsData.Instrument.Name, tsData.TradePrice, tsData.TradeQuantity);
                // only timestamp requires ''  TODO: sending id instead of string to column 6 of type DB:VARCHAR
                cmd.Parameters.AddWithValue("timestamp", NpgsqlTypes.NpgsqlDbType.Timestamp,  tsData.TimeStamp);
                cmd.Parameters.AddWithValue("contract_id", this.ContractIDList[tsData.Instrument.Key.SeriesKey]);
                cmd.Parameters.AddWithValue("relative", Relative(pd._front_month, tsData.Instrument.InstrumentDetails.ExpirationDate.ToDateTime()));
                cmd.Parameters.AddWithValue("price", PriceCheck(tsData.TradePrice, pd));
                cmd.Parameters.AddWithValue("qty", tsData.TradeQuantity.ToInt());
            }
            ExecuteSQL_NonQuery(cmd);
        }

        private const string COLUMNS_SPREADS = "(time_stamp,contract_id_1,contract_id_2,relative_contract_1,relative_contract_2,price,volume) ";
        // not a security issue.  pd._spreadTableName is created in app, not from user input. (SQL Injection alert)
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities")]
        internal void InsertSpreadData(ReadOnlyCollection<TimeAndSalesData> data, Instrument i, ProductDetails pd)
        {
            Leg front = i.GetLegs()[0];
            Leg back = i.GetLegs()[1];

            NpgsqlCommand cmd = new NpgsqlCommand
            {
                Connection = this.dbrw,
                CommandText = String.Format("INSERT INTO {0} {1} VALUES (@time_stamp,@contract_id_1,@contract_id_2,@relative_contract_1,@relative_contract_2,@price,@volume);", pd._spreadTableName, COLUMNS_SPREADS)
            };

            foreach (TimeAndSalesData tsData in data)
            {
                log.Debug("{0}: {1} {2}",tsData.Instrument.Name, tsData.TradePrice, tsData.TradeQuantity);
                cmd.Parameters.AddWithValue("time_stamp", NpgsqlTypes.NpgsqlDbType.Timestamp, tsData.TimeStamp);
                cmd.Parameters.AddWithValue("contract_id_1", ContractIDList[front.Instrument.Key.SeriesKey]);
                cmd.Parameters.AddWithValue("contract_id_2", ContractIDList[back.Instrument.Key.SeriesKey]);
                cmd.Parameters.AddWithValue("relative_contract_1", Relative(pd._front_month, front.Instrument.InstrumentDetails.ExpirationDate.ToDateTime()));
                cmd.Parameters.AddWithValue("relative_contract_2", Relative(pd._front_month, back.Instrument.InstrumentDetails.ExpirationDate.ToDateTime()));
                cmd.Parameters.AddWithValue("price", PriceCheck(tsData.TradePrice, pd));
                cmd.Parameters.AddWithValue("volume", tsData.TradeQuantity.ToInt());
            }
            ExecuteSQL_NonQuery(cmd);
        }
        
        /// <summary>
        /// Execute sql commands with error catching
        /// </summary>
        /// <param name="cmd"></param>
        /// <returns></returns>
        private int ExecuteSQL_NonQuery(NpgsqlCommand cmd)
        {
            if (!cmd.Connection.State.Equals(ConnectionState.Open)) OpenDB(cmd.Connection);
            if (sqlcmdQ.Count > 0)
            {
                sqlcmdQ.Enqueue(cmd);
                return ReProcessQ(sqlcmdQ);
            }
            else
            {
                try
                {
                    return cmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    sqlcmdQ.Enqueue(cmd);
                    string sql = cmd.CommandText;
                    // cmd has been executed and failed before reaching this line, therefore we know there is one statement [0]
                    foreach (NpgsqlParameter item in cmd.Statements[0].InputParameters)
                    {
                        if (item.Value.GetType().Equals(typeof(DateTime)))
                        {
                            sql = sql.Replace('@' + item.ParameterName, "'" + ((DateTime)item.Value).ToString("yyyy-MM-dd HH:mm:ss.fff") + "'");
                        }
                        else
                        {
                            sql = sql.Replace('@' + item.ParameterName, item.Value.ToString());
                        }
                    }

                    if (sql.StartsWith("INSERT"))
                    {
                        FailedInsert.Info(sql);
                    }
                    else
                    {
                        log.Error("MSG: {0}", ex.Message);
                        log.Error(sql);
                    }

                    // query will return 0 or higher. return -1 to inform of error.
                    log.Error(ex);
                    return -1;
                }
            }
        }

        private int ReProcessQ(Queue<NpgsqlCommand> q)
        {
            int count = 0;
            if (this.dbrw.State.Equals(ConnectionState.Open))
            {
                while (q.Count > 0)
                {
                    try
                    {
                        // execute FIFO query
                        count += q.Peek().ExecuteNonQuery();
                        // remove from queue if successful
                        q.Dequeue();
                    }
                    catch (Exception ex)
                    {
                        log.Error(ex);
                        return -1;
                    }
                }
            }
            return count;
        }

        private object Execute_Scalar(NpgsqlCommand cmd)
        {
            try
            {
                return cmd.ExecuteScalar();
            }
            catch (Exception ex)
            {
                log.Error("MSG: {0}", ex.Message);
                log.Error("SQL: {0}", cmd.Statements[0].SQL);
                foreach (var item in cmd.Statements[0].InputParameters)
                {
                    log.Error("{0}={1}", item.ParameterName, item.Value);                    
                }
                log.Error(ex);
                // query will return first column of first row.  0 or higher. return -1 to inform of error.
                return null;
            }
        }

        /// <summary>
        /// Process data for insertion into database
        /// </summary>
        /// <param name="expiry"></param>
        /// <param name="start"></param>
        /// <returns></returns>
        private int ContractId(string expiry, int start)
        {
            DateTime contract = DateTime.ParseExact(expiry, "MMMyy", new System.Globalization.CultureInfo("en-US"));
            return (contract.Year - DateTime.Now.Year) * 12 + (contract.Month - DateTime.Now.Month) + start;
        }

        private Double PriceCheck(Price p, ProductDetails pd)
        {
            return (p.ToTicks() * pd._tick_currency_value) / pd._contract_size;
        }

        private int Relative(DateTime front, DateTime contract)
        {
            return (contract.Year - front.Year) * 12 + (contract.Month - front.Month) + 1;
        }

        private string CleanTTExchange(string exchange_name)
        {
            if (exchange_name.ToUpper().Equals("ICE"))
            {
                return "ICE_IPE";
            }
            return exchange_name.ToUpper();
        }

        private string CleanFileName(string exchange, string ticker)
        {
            string e = exchange.Trim().ToLower();
            //string t = ticker.Trim().ToLower().Replace('-', '_').Replace(' ', '_');  
            string t = string.Join("", ticker.Split(new char[] { '-', ' ' }, StringSplitOptions.RemoveEmptyEntries)).ToLower();
            return e + "_" + t;
        }

        /// <summary>
        /// Database event handlers 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Db_StateChange(object sender, StateChangeEventArgs e)
        {
            Configuration.CrossThreadUILbUpdate("Database Connection State: " + e.CurrentState);
            
            if (e.CurrentState.Equals(ConnectionState.Open))
            {
                log.Info("Opened connection to PostgreSQL Version: {0}", dbrw.PostgreSqlVersion);
            }
        }

        private void Dbrw_Disposed(object sender, EventArgs e)
        {
            log.Info("DB Connection is Disposed.");
        }

        private void Dbrw_Notice(object sender, NpgsqlNoticeEventArgs e)
        {
            if (e.Notice != null && !e.Notice.MessageText.Contains("skipping"))
            {
                log.Info("DB Notice: {0}", e.Notice.MessageText);
                if (e.Notice.Detail != null)
                { log.Info(e.Notice.Detail); }
            }
        }

        private void Dbrw_Notification(object sender, NpgsqlNotificationEventArgs e)
        {
            log.Info("DB Notification: {0}", e.Condition);
            if (e.AdditionalInformation != null)
            { log.Info(e.AdditionalInformation); }
        }

        /// <summary>
        /// Dispose Method to ensure database connection is closed
        /// </summary>
        public void Dispose()
        {
            if (dbrw != null)
            {
                dbrw.Close();
                dbrw.Dispose();
            }
        }
    }
}
