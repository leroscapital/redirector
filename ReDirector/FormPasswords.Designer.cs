﻿namespace ReDirector
{
    partial class FormPasswords
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormPasswords));
            this.button1 = new System.Windows.Forms.Button();
            this.password_dbrw = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.password_tt = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.password_dbro = new System.Windows.Forms.TextBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Enabled = false;
            this.button1.Location = new System.Drawing.Point(6, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(153, 43);
            this.button1.TabIndex = 0;
            this.button1.Text = "Continue";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // password_dbrw
            // 
            this.password_dbrw.Location = new System.Drawing.Point(6, 19);
            this.password_dbrw.Name = "password_dbrw";
            this.password_dbrw.Size = new System.Drawing.Size(248, 20);
            this.password_dbrw.TabIndex = 0;
            this.password_dbrw.TextChanged += new System.EventHandler(this.DB_Password_TextChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.password_dbrw);
            this.groupBox1.Location = new System.Drawing.Point(12, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(260, 50);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Read / Write Database Password";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.password_tt);
            this.groupBox2.Location = new System.Drawing.Point(14, 131);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(258, 53);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "TT Password";
            // 
            // password_tt
            // 
            this.password_tt.Location = new System.Drawing.Point(5, 19);
            this.password_tt.Name = "password_tt";
            this.password_tt.Size = new System.Drawing.Size(248, 20);
            this.password_tt.TabIndex = 0;
            this.password_tt.TextChanged += new System.EventHandler(this.TT_Password_TextChanged);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.password_dbro);
            this.groupBox3.Location = new System.Drawing.Point(14, 70);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(257, 55);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Read Only Database Password";
            // 
            // password_dbro
            // 
            this.password_dbro.Location = new System.Drawing.Point(4, 19);
            this.password_dbro.Name = "password_dbro";
            this.password_dbro.Size = new System.Drawing.Size(248, 20);
            this.password_dbro.TabIndex = 0;
            this.password_dbro.TextChanged += new System.EventHandler(this.Password_dbro_TextChanged);
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.CheckAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.checkBox1.Location = new System.Drawing.Point(157, 19);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(96, 31);
            this.checkBox1.TabIndex = 1;
            this.checkBox1.Text = "Save Passwords?";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.button1);
            this.groupBox4.Controls.Add(this.checkBox1);
            this.groupBox4.Location = new System.Drawing.Point(14, 190);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(258, 65);
            this.groupBox4.TabIndex = 3;
            this.groupBox4.TabStop = false;
            // 
            // FormPasswords
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(283, 261);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormPasswords";
            this.Text = "Enter Passwords";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormPasswords_FormClosing);
            this.Load += new System.EventHandler(this.FormPasswords_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox password_dbrw;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox password_tt;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox password_dbro;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.GroupBox groupBox4;
    }
}