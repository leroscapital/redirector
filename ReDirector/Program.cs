﻿using System;
using System.Threading;
using System.Windows.Forms;

namespace ReDirector
{
    using NLog;

    static class Program
    {
        static Logger log = LogManager.GetCurrentClassLogger();

        [STAThread]
        static void Main(string[] args)
        {
            Thread.CurrentThread.Name = "MAIN";
            Configuration.StartupTime = DateTime.Now;
            log.Info("Started at: {0}  With parameters: [{1}]", Configuration.StartupTime, String.Join(", ", args));
            log.Info("available CPUs: {0}", Environment.ProcessorCount);
            log.Info("OS {0} 64bit: {1}/{2}", Environment.OSVersion, Environment.Is64BitOperatingSystem, Environment.Is64BitProcess);
            log.Info("CLR Version: {0}", Environment.Version);
            log.Trace("Managed Thread ID: {0} Name: {1}", Thread.CurrentThread.ManagedThreadId, Thread.CurrentThread.Name);
            TTAPIArchitectureCheck.test();
            ProgramOptionParser.PopulateArgDict(args);

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Configuration.frmMain = new FormMainUI();
            Application.Run(Configuration.frmMain);
        }

    }
}
