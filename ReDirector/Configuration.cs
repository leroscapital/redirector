﻿using System;
using System.Data;
using System.IO;

namespace ReDirector
{
    using System.Windows.Forms;
    using NLog;

    public static class Configuration
    {
        static DataSet _conf = new DataSet("Config");
        const string _TBL_SCHEDULE = "Schedule";
        const string _COL_START = "Start";
        const string _COL_STOP= "Stop";
        const string _COL_ACTIVE = "Active";

        const string _TBL_PASSWORDS = "Passwords";
        const string _COL_RODB = "RODB";
        const string _COL_RWDB = "RWDB";
        const string _COL_DATA = "TTPW";

        const string _TBL_RWDB = "RWDB_Connection";
        const string _TBL_RODB = "RODB_Connection";
        const string _C_HOST = "Host";
        const string _C_USER = "User";
        const string _C_PORT = "Port";
        const string _C_NAME = "Name";

        const string _TBL_DATAFEED = "DataFeed";
        // _C_USER column in this tabel

        static Logger log = LogManager.GetCurrentClassLogger();
#if DEBUG
        static string _FILENAME  = Environment.CurrentDirectory + @"\config\testConfig.xml";
#else
        static string _FILENAME  = Environment.CurrentDirectory + @"\config\Config.xml";
#endif
        
        internal static DateTime ShutDownTime = DateTime.MaxValue;   // initialize value. value must be overwritten 
        internal static DateTime StartupTime = DateTime.MinValue;
        internal static bool UseSchedule = false;
        internal static FormMainUI frmMain = null;

        static Configuration()
        {
            log.Trace(".");
            LoadConfig(_FILENAME);
            ShutDownTime = GetStopTime();
            StartupTime = DateTime.Now;
            UseSchedule = GetScheduleActive();
            log.Info("Automatic shutdown scheduled for {0} Active: {1}", ShutDownTime, UseSchedule);
        }

        static void LoadConfig(string filename)
        {
            log.Debug(".");
            bool createDataSet = false;
            if (File.Exists(filename))
            {
                try
                {
                    _conf.ReadXml(filename);
                    PrintDataSet(_conf);
                    DecryptPasswords(_conf, _TBL_PASSWORDS);
                }
                catch (Exception ex)
                {
                    log.Info("Can't read configuration file {0}.  Creating default configuration.", filename);
                    log.Error(ex);
                    createDataSet = true;
                }
            }
            VerifyDataSetStructure(createDataSet);
        }

        static void VerifyDirectoryStructure(string fileName)
        {
                string d = Path.GetDirectoryName(fileName);
                log.Debug("Using configuration in {0}", d);
                if (!Directory.Exists(d))
                {
                    try
                    {
                        Directory.CreateDirectory(d);
                    }
                    catch (Exception ex)
                    {
                        log.Error(ex);
                    }
                }
        }

        internal static void WriteConfig()
        {
            log.Debug(".");
            VerifyDirectoryStructure(_FILENAME);
            PrintDataSet(_conf);
            EncryptPasswords(_conf, _TBL_PASSWORDS);
            _conf.AcceptChanges();
            _conf.WriteXml(_FILENAME);
            DecryptPasswords(_conf, _TBL_PASSWORDS);
        }

        static void VerifyDataSetStructure(bool forceCreate)
        {
            log.Debug(".");
            if (!_conf.Tables.Contains(_TBL_SCHEDULE) || forceCreate)
            {
                DataTable schedule = new DataTable(_TBL_SCHEDULE);
                DataColumn active = new DataColumn
                {
                    ColumnName = _COL_ACTIVE,
                    DefaultValue = "False"
                };
                schedule.Columns.Add(active);

                DataColumn start = new DataColumn
                {
                    ColumnName = _COL_START,
                    DefaultValue = "17:55:00"
                };
                schedule.Columns.Add(start);

                DataColumn stop = new DataColumn
                {
                    ColumnName = _COL_STOP,
                    DefaultValue = "17:05:00"
                };
                schedule.Columns.Add(stop);
                schedule.Rows.Add(schedule.NewRow());

                _conf.Tables.Add(schedule);
                _conf.AcceptChanges();
            }

            if (!_conf.Tables.Contains(_TBL_PASSWORDS) || forceCreate)
            {
                DataTable passwords = new DataTable(_TBL_PASSWORDS);
                passwords.Columns.Add(new DataColumn
                {
                    ColumnName = _COL_RODB,
                    DefaultValue = ""
                });
                passwords.Columns.Add(new DataColumn
                {
                    ColumnName =_COL_RWDB,
                    DefaultValue = ""
                });
                passwords.Columns.Add(new DataColumn
                {
                    ColumnName = _COL_DATA,
                    DefaultValue = ""
                });
                passwords.Rows.Add(passwords.NewRow());
                _conf.Tables.Add(passwords);
                _conf.AcceptChanges();
            }

            if (!_conf.Tables.Contains(_TBL_RWDB) || forceCreate)
            {
                DataTable dt = new DataTable(_TBL_RWDB);
                dt.Columns.Add(new DataColumn
                {
                   ColumnName = _C_HOST,
                   DefaultValue = ""
                });
                dt.Columns.Add(new DataColumn
                {
                   ColumnName = _C_PORT,
                   DefaultValue = "5432"
                });
                dt.Columns.Add(new DataColumn
                {
                   ColumnName = _C_NAME,
                   DefaultValue = ""
                });
                dt.Columns.Add(new DataColumn
                {
                   ColumnName = _C_USER,
                   DefaultValue = ""
                });
                dt.Rows.Add(dt.NewRow());
                _conf.Tables.Add(dt);
                _conf.AcceptChanges();
            }
            if (!_conf.Tables.Contains(_TBL_RODB) || forceCreate)
            {
                DataTable dt = new DataTable(_TBL_RODB);
                dt.Columns.Add(new DataColumn
                {
                   ColumnName = _C_HOST,
                   DefaultValue = ""
                });
                dt.Columns.Add(new DataColumn
                {
                   ColumnName = _C_PORT,
                   DefaultValue = "5432"
                });
                dt.Columns.Add(new DataColumn
                {
                   ColumnName = _C_NAME,
                   DefaultValue = ""
                });
                dt.Columns.Add(new DataColumn
                {
                   ColumnName = _C_USER,
                   DefaultValue = ""
                });
                dt.Rows.Add(dt.NewRow());
                _conf.Tables.Add(dt);
                _conf.AcceptChanges();
            }
            if (!_conf.Tables.Contains(_TBL_DATAFEED) || forceCreate)
            {
                DataTable dt = new DataTable(_TBL_DATAFEED);
                dt.Columns.Add(new DataColumn
                {
                    ColumnName = _C_USER,
                    DefaultValue = ""
                });
                dt.Rows.Add(dt.NewRow());
                _conf.Tables.Add(dt);
                _conf.AcceptChanges();
            }
        }

        static void DecryptPasswords(DataSet ds, string tableName)
        {
            DataTable dt = ds.Tables[tableName];
            log.Debug(dt.Columns.Count);
            foreach (DataColumn c in dt.Columns)
            {
                log.Debug(c.ColumnName);
                string encoded_password = dt.Rows[0][c].ToString();
                string decoded_password = Utility.Base64Decode(encoded_password);
                dt.Rows[0][c] = decoded_password;
            }
        }

        static void EncryptPasswords(DataSet ds, string tableName)
        {
            DataTable dt = ds.Tables[tableName];
            foreach (DataColumn c in dt.Columns)
            {
                log.Debug(c.ColumnName);
                string decoded_password = dt.Rows[0][c].ToString();
                string encoded_password = Utility.Base64Encode(decoded_password);
                dt.Rows[0][c] = encoded_password;
            }

        }

        internal static string GetTTUser()
        {
            return _conf.Tables[_TBL_DATAFEED].Rows[0][_C_USER].ToString();
        }
        internal static string GetTTPassword()
        {
            return _conf.Tables[_TBL_PASSWORDS].Rows[0][_COL_DATA].ToString();
        }
        
        internal static bool GetScheduleActive()
        {
            log.Debug(".");
            return GetBoolFromDataSet(_COL_ACTIVE);
        }

        internal static DateTime GetStartTime()
        {
            TimeSpan start = GetTimeFromDataSet(_TBL_SCHEDULE, _COL_START);
            log.Debug(message: start.ToString());
            if (start > DateTime.Now.TimeOfDay)
            {
                return DateTime.Today + start;
            }
            else
            { return DateTime.Today.AddDays(1) + start; }
        }

        internal static DateTime GetStopTime()
        {
            TimeSpan stop = GetTimeFromDataSet(_TBL_SCHEDULE ,_COL_STOP);
            log.Debug(message: stop.ToString());
            if (stop > DateTime.Now.TimeOfDay)
            {
                return DateTime.Today + stop;
            }
            else
            {
                return DateTime.Today.AddDays(1) + stop;
            }
        }

        static bool GetBoolFromDataSet(string columnName)
        {
            log.Debug(".");
            try
            {
                return Convert.ToBoolean(_conf.Tables[_TBL_SCHEDULE].Rows[0][columnName].ToString());
            }
            catch (FormatException fEx)
            { log.Error("incorrect data in Config.xml: {0}", fEx); }
            return false;
        }

        static TimeSpan GetTimeFromDataSet(string tableName, string columnName)
        {
            log.Debug(".");
            try
            {
                return TimeSpan.Parse(_conf.Tables[tableName].Rows[0][columnName].ToString());
            }
            catch (Exception ex)
            {
                log.Error(ex);
            }
            // if error return zero
            return TimeSpan.Zero;
        }

        // set Schedule Data
        static string taskName = "ReDirector";
        internal static void SetScheduleActivationState(bool b, TimeSpan start, TimeSpan stop)
        {
            SetConfigData(_TBL_SCHEDULE, _COL_ACTIVE, b);
            SetConfigData(_TBL_SCHEDULE, _COL_START, start);
            SetConfigData(_TBL_SCHEDULE, _COL_STOP, stop);

            ShutDownTime = GetStopTime();
            UseSchedule = b;

            if (b)
            {
                Utility.GetAndChangeTask(taskName, start);
                string message = String.Format("Automatic shutdown scheduled for {0}", ShutDownTime);
                CrossThreadUILbUpdate(message);
                log.Info(message);
            }
            else
            {
                Utility.GetAndDeleteTask(taskName);
                string message = "Automatic Shutdown Schedule NOT active.";
                CrossThreadUILbUpdate(message);
                log.Info(message);
            }

            log.Info("Schedule Activated?: {0}", b);
            log.Info("Start Time set to: {0}", start);
            log.Info("Stop Time set to: {0}", stop);
        }
        
        static void SetConfigData(string tableName, string column, object data)
        {
            log.Debug(".");
            try
            {
                DataTable tbl = _conf.Tables[tableName];
                if (tbl.Rows.Count == 0)
                {
                    DataRow r = tbl.NewRow();
                    r[column] = data.ToString();
                    tbl.Rows.Add(r);
                }
                else
                {
                    tbl.Rows[0][column] = data.ToString();
                }
                tbl.AcceptChanges();
            }
            catch (Exception ex)
            { log.Error(ex); }

            if (column.Equals(_COL_STOP)) { ShutDownTime = GetStopTime(); }
            if (column.Equals(_COL_ACTIVE)) { UseSchedule = GetScheduleActive(); }
        }

        static void SetDefaultValues()
        {
            // Set default values
            SetScheduleActivationState(false, TimeSpan.Parse("17:55:00"), TimeSpan.Parse("17:01:00"));
            _conf.WriteXml(_FILENAME);
        }

        static void PrintDataSet(DataSet ds)
        {
#if DEBUG
            log.Debug("----------------------------------------------");
            log.Debug("Tables in '{0}' DataSet.", ds.DataSetName);
            log.Debug("----------------------------------------------");
            foreach (DataTable dt in ds.Tables)
            {
                log.Debug("{0} Table.", dt.TableName);
                string header = "";
                foreach (DataColumn col in dt.Columns)
                { header += ("[" + col.ColumnName + "]\t"); }
                log.Debug(header);
                string row_data = "";
                foreach  (DataRow row in dt.Rows)
                {
                    foreach (DataColumn col in dt.Columns)
                    {
                        row_data += (row[col] + "  \t");
                    }
                    log.Debug(row_data);
                    row_data = "";
                }
                log.Debug("----------------------------------------------");
            }
#endif
        }

        internal static bool IsFullyConfigured()
        {
            DataSet ds = _conf;
            foreach (DataTable dt in ds.Tables)
            {
                foreach  (DataRow row in dt.Rows)
                {
                    foreach (DataColumn col in dt.Columns)
                    {
                        if (row[col].Equals(null) || row[col].ToString().Length == 0)
                        {
                            return false;
                        }
                    }
                }
            }
            return true;
        }


        internal static void TestConfiguration()
        {
             string[] list = { _TBL_DATAFEED, _TBL_RODB, _TBL_RWDB };
            if (VerifySettings(_conf, list))
            {
                VerifyPasswords();
            }
            else
            {
                using (FormUserSettings getSettings = new FormUserSettings())
                {
                    getSettings.ShowDialog();
                }
            }
        }

        static void VerifyPasswords()
        {
            if (!VerifyDataLoaded(_conf, _TBL_PASSWORDS))
            {
                using (FormPasswords getPasswords = new FormPasswords())
                {
                    getPasswords.ShowDialog();
                };
            }
        }

        static bool VerifySettings(DataSet ds, string[] tableNames)
        {
            try
            {
                foreach (string tableName in tableNames)
                {
                    DataTable dt = ds.Tables[tableName];
                    if (dt.Rows.Count == 0) return false;

                    foreach (DataRow r in dt.Rows)
                    {
                        foreach (DataColumn c in dt.Columns)
                        {
                            if (r[c].ToString().Length == 0) return false;
                        }
                    }
                }
            }
            catch (Exception ex)
            { log.Error(ex); }
            return true;
        }

        static bool VerifyDataLoaded(DataSet ds, string tableName)
        {
            if (ds.Tables[tableName].Rows.Count.Equals(0)) return false;

            foreach (DataColumn c in ds.Tables[tableName].Columns)
            {
                if (ds.Tables[tableName].Rows[0][c].ToString().Length == 0) return false; 
            }
            return true;
        }

        internal static void AddDataBinding(TextBox txtBox, string table_column)
        {
            // table_column = "tableName.columnName"
            txtBox.DataBindings.Add(new Binding("Text", _conf, table_column)); 
        }
        internal static void AddDataBinding(DateTimePicker dtp, string table_column)
        {
            dtp.DataBindings.Add(new Binding("Value", _conf, table_column)); 
        }
        internal static void AddDataBinding(CheckBox cb, string table_column)
        {
            cb.DataBindings.Add(new Binding("Checked", _conf, table_column)); 
        }

        internal static string GetDBConnection(string permissions = "rw")
        {
            if (permissions.Equals("ro"))
            {
                return String.Format("Host={0};Username={1};Password={2};Database={3};Port={4}",
                    _conf.Tables[_TBL_RODB].Rows[0][_C_HOST].ToString(),
                    _conf.Tables[_TBL_RODB].Rows[0][_C_USER].ToString(),
                    _conf.Tables[_TBL_PASSWORDS].Rows[0][_COL_RODB].ToString(),
                    _conf.Tables[_TBL_RODB].Rows[0][_C_NAME].ToString(),
                    _conf.Tables[_TBL_RODB].Rows[0][_C_PORT].ToString()
                    );
            }
            else // rw
            {
                return String.Format("Host={0};Username={1};Password={2};Database={3};Port={4}",
                    _conf.Tables[_TBL_RWDB].Rows[0][_C_HOST].ToString(),
                    _conf.Tables[_TBL_RWDB].Rows[0][_C_USER].ToString(),
                    _conf.Tables[_TBL_PASSWORDS].Rows[0][_COL_RWDB].ToString(),
                    _conf.Tables[_TBL_RWDB].Rows[0][_C_NAME].ToString(),
                    _conf.Tables[_TBL_RWDB].Rows[0][_C_PORT].ToString()
                    );
            }
        }

        internal static void CrossThreadUILbUpdate(string message)
        {
            try
            {
                frmMain.BeginInvoke(frmMain.UpdateMainUIMessage, message);
            }
            catch (Exception ex)
            {
                log.Info(message);
                log.Error(ex.Message);
                foreach (var item in ex.Data)
                {
                    log.Error("{0}: {1}", item, ex.Data[item]);
                }
            }
        }

        internal static void UpdateUIStatusline(string message)
        {
            try
            {
                frmMain.BeginInvoke(frmMain.GetInserts, message);
            }
            catch (Exception ex)
            {
                log.Info(message);
                log.Error(ex.Message);
                foreach (var item in ex.Data)
                {
                    log.Error("{0}: {1}", item, ex.Data[item]);
                }
            }
        }

        internal static void UpdateCounts(int futures, int spreads)
        {
            try
            {
                frmMain.BeginInvoke(frmMain.GetCounts, futures, spreads);
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                foreach (var item in ex.Data)
                {
                    log.Error("{0}: {1}", item, ex.Data[item]);
                }
            }
        }

        internal static void ShutDown(bool b)
        {
            try
            {
                frmMain.BeginInvoke(frmMain.GetStartShutdown, b);
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                foreach (var item in ex.Data)
                {
                    log.Error("{0}: {1}", item, ex.Data[item]);
                }
            }
        }
    }

}
