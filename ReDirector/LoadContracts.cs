﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace TTAPI2MYSQL
{
    using TradingTechnologies.TTAPI;
    using NLog;

    class LoadContracts
    {
        private static Logger log = LogManager.GetCurrentClassLogger();
        public List<ProductDetails> loaded  = new List<ProductDetails>();

        //Constructor - Load contracts from file
        public LoadContracts(string filename)
        {
            if (File.Exists(filename))
            {
                read(filename);
            }
            else
            {
                log.Fatal(filename + " config file is not found! Hit any key to exit application.");
                Console.ReadKey();
                Environment.Exit(1);
            }
        }

        private void read(string fn)
        {
            // # marks a comment in the contracts file
            // this is designed specifically to address the product list with exchange headers in []
            // followed by a list of tickers one per line           
            string line, exchange = null;
            char[] charsToTrim = { '[', ']', ' ' };
            char[] charsToSplit = { ':', ',' };

            StreamReader f = new StreamReader(fn);
            int lineCount = 0;
            while ((line = f.ReadLine()) != null)
            {
                lineCount++;
                // is this line an exchange identifier?
                if (line.Contains('['))
                {
                    exchange = line.Trim(charsToTrim);
                }
                else if (!line.Contains('#'))  // is this line not a comment -- expected ticker and optional type
                {
                    string ticker, cTypes = null;
                    string[] s = line.Split(charsToSplit);
                    if (s.Length > 0) // skip blank lines
                    {
                        ticker = s[0].Trim(charsToTrim);
                        if (s.Length == 2)
                        {
                            //contract product types in FSO = Future, Spread, Option
                            cTypes = s[1].Trim(charsToTrim).ToUpper();
                        }
                        else if (s.Length == 1)
                        {
                            cTypes = "FSO";
                        }
                        else
                        {
                            log.Error("Invalid format in {0} on line {1}", f, lineCount);
                            log.Info("Hit any key to exit");
                            Console.Read();
                            Environment.Exit(1);
                        }

                        foreach (char t in cTypes)
                        {
                            ProductType pt;
                            switch (t)
                            {
                                case 'F':
                                    pt = ProductType.Future;
                                    break;
                                case 'S':
                                    pt = ProductType.Spread;
                                    break;
                                case 'O':
                                    pt = ProductType.Option;
                                    break;
                                default:
                                    pt = ProductType.Invalid;
                                    break;
                            }
                            loaded.Add(new ProductDetails(exchange, pt, ticker));
                        }
                    }
                }
            }
            f.Close();
            log.Info("Contract definitions loaded");
        }
        
        //private MarketKey getMarket(string exchange)
        //{
        //    switch (exchange.ToUpper())
        //    {
        //        case "CME":
        //            return MarketKey.Cme;
        //        case "ICE_IPE":
        //            return MarketKey.Ice;
        //        default:
        //            return MarketKey.TTSIM;
        //    }
        //}
    }
}
