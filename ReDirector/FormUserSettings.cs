﻿using System;
using System.Windows.Forms;

namespace ReDirector
{
    using NLog;

    public partial class FormUserSettings : Form
    {
        private static Logger log = LogManager.GetCurrentClassLogger();
        private TimeSpan start;
        private TimeSpan stop;
        private bool ScheduleActive;
        
        public FormUserSettings()
        {
            InitializeComponent();
            SetupForm();
        }

        private void SetupForm()
        {
            this.ActiveControl = this.txtRWDB_Host;
            Configuration.AddDataBinding(txtRWDB_Host, "RWDB_Connection.Host"); 
            Configuration.AddDataBinding(txtRWDB_Name, "RWDB_Connection.Name");
            Configuration.AddDataBinding(txtRWDB_Port, "RWDB_Connection.Port"); 
            Configuration.AddDataBinding(txtRWDB_User, "RWDB_Connection.User"); 
            Configuration.AddDataBinding(txtrwdb_password, "Passwords.RWDB"); 

            Configuration.AddDataBinding(txtrodb_host, "RODB_Connection.Host"); 
            Configuration.AddDataBinding(txtrodb_name, "RODB_Connection.Name"); 
            Configuration.AddDataBinding(txtrodb_port, "RODB_Connection.Port"); 
            Configuration.AddDataBinding(txtrodb_user, "RODB_Connection.User"); 
            Configuration.AddDataBinding(txtrodb_password, "Passwords.RODB"); 

            Configuration.AddDataBinding(txtTTUser, "Datafeed.User"); 
            Configuration.AddDataBinding(txtTTpassword, "Passwords.TTPW");

            //Configuration.AddDataBinding(dateTimePicker1, "Schedule.Start");
            //Configuration.AddDataBinding(dateTimePicker2, "Schedule.Stop");
            //Configuration.AddDataBinding(checkBox1, "Schedule.Active");
            GetSchedule();
        }

        private void SaveSettings_Click(object sender, EventArgs e)
        {
            SaveSchedule();
            Configuration.WriteConfig();
            this.Close();
        }

        private void FormUserSettings_Load(object sender, EventArgs e)
        {

        }

        private void FormUserSettings_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (Configuration.IsFullyConfigured())
            {
                Configuration.frmMain.Enable_Startup(true);
                Configuration.frmMain.UpdateMainUIMessage("All settings are configured.");
                log.Info("All settings are configured.");
            }
            else
            {
                Configuration.frmMain.Enable_Startup(false);
                Configuration.frmMain.UpdateMainUIMessage("Settings are incomplete. Click 'Edit Settings' button.");
                log.Info("Settings are incomplete. Click 'Edit Settings' button.");
            }

        }

        private void GetSchedule()
        {
            this.dateTimePicker1.Value = Configuration.GetStartTime();
            this.dateTimePicker2.Value = Configuration.GetStopTime();
            this.checkBox1.Checked = Configuration.GetScheduleActive(); 
            this.start = this.dateTimePicker1.Value.TimeOfDay;
            this.stop = this.dateTimePicker2.Value.TimeOfDay;
            this.ScheduleActive = this.checkBox1.Checked; 
        }

        private void SaveSchedule()
        {
            Configuration.SetScheduleActivationState(this.checkBox1.Checked,
                this.dateTimePicker1.Value.TimeOfDay,
                this.dateTimePicker2.Value.TimeOfDay
                );
        }
    }
}
