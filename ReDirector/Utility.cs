﻿using System;
using System.Data;
using System.Data.Common;

namespace ReDirector
{
    using NLog;
    using Microsoft.Win32.TaskScheduler;
    using Microsoft.Win32.TaskScheduler.Fluent;
    using System.Windows.Forms;

    public static class Utility
    {
        static Logger log = LogManager.GetCurrentClassLogger();

        // https://docs.microsoft.com/en-us/dotnet/framework/data/adonet/obtaining-a-dbproviderfactory
        static DataTable GetProviderFactoryClasses()
        {
            // Retrieve the installed providers and factories.
            DataTable table = DbProviderFactories.GetFactoryClasses();

            // Display each row and column value.
            foreach (DataRow row in table.Rows)
            {
                foreach (DataColumn column in table.Columns)
                {
                    log.Info(row[column]);
                }
            }
            return table;
        }

        // https://stackoverflow.com/a/11743162/2472798
        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }

        public static string Base64Decode(string base64EncodedData)
        {
            try
            {
                var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
                return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                log.Error(ex);
                return String.Empty;
            }
        }

        public static void ImportXMLTask(string taskName, TimeSpan newTime)
        {
            using (TaskService ts = new TaskService())
            {
                try
                {
                    TaskDefinition td = TaskService.Instance.NewTaskFromFile(Environment.CurrentDirectory + @"\config\ReDirectorTask.xml");
                    td.RegistrationInfo.Author = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
                    td.RegistrationInfo.Date = DateTime.Now;

                    td.Principal.Id = System.Security.Principal.WindowsIdentity.GetCurrent().User.ToString();
                    td.Principal.LogonType = TaskLogonType.InteractiveToken;
                    // this fails to import
                    //td.Principal.RunLevel = TaskRunLevel.Highest;

                    td.Triggers[0].StartBoundary = DateTime.Today + newTime;

                    ts.RootFolder.RegisterTaskDefinition(taskName, td);
                }
                catch (Exception ex)
                {
                   log.Debug(ex);
                }
                
            }
        }

        

        public static void GetAndChangeTask(string taskName, TimeSpan newTime)
        {
            log.Trace(".");
            using (TaskService ts = new TaskService())
            {
                // Retrieve the task, change the trigger and re-register it.
                // A taskName by itself assumes the root folder (e.g. "MyTask")
                // A taskName can include folders (e.g. "MyFolder\MySubFolder\MyTask")
                Task t = ts.GetTask(taskName);
                if (t == null)
                {
                    try
                    {
                        ImportXMLTask(taskName, newTime);
                        log.Info("Task Imported into Task Scheduler.");
                    }
                    catch (Exception ex)
                    { log.Error(ex); }
                }
                else
                {
                    try
                    {
                        t.Definition.Triggers[0].StartBoundary = DateTime.Today + newTime;
                        t.RegisterChanges();
                        log.Info("Task start time updated.");
                    }
                    catch (Exception ex)
                    { log.Error(ex); }
                }
            }
        }

        public static void CreateSimpleTask()
        {
            // Get the service on the local machine
            using (TaskService ts = new TaskService())
            {
                // Create a new task definition and assign properties
                TaskDefinition td = ts.NewTask();
                td.RegistrationInfo.Description = "Start ReDirector on standard trading days, Sun through Thursday";
                td.Principal.LogonType = TaskLogonType.InteractiveToken;

                // Add a trigger that will fire the task at this time every other day
                DailyTrigger dt = (DailyTrigger)td.Triggers.Add(new DailyTrigger(2));
                dt.Repetition.Duration = TimeSpan.FromHours(4);
                dt.Repetition.Interval = TimeSpan.FromHours(1);

                // Add a trigger that will fire every week on Friday
                td.Triggers.Add(new WeeklyTrigger
                {
                    StartBoundary = DateTime.Today
                   + TimeSpan.FromHours(2),
                    DaysOfWeek = DaysOfTheWeek.Friday
                });

                // Add an action that will launch Notepad whenever the trigger fires
                td.Actions.Add(new ExecAction("notepad.exe", "c:\\test.log", null));

                // Register the task in the root folder
                const string taskName = "Test";
                ts.RootFolder.RegisterTaskDefinition(taskName, td);
            }
        }

        public static void GetAndDeleteTask(string taskName)
        {
            log.Trace(".");
            using (TaskService ts = new TaskService())
            {
                // Retrieve the task, change the trigger and re-register it.
                // A taskName by itself assumes the root folder (e.g. "MyTask")
                // A taskName can include folders (e.g. "MyFolder\MySubFolder\MyTask")
                Task t = ts.GetTask(taskName);

                log.Debug("gotTask");
                if (t == null) return;

                // Check to make sure account privileges allow task deletion
                //var identity = System.Security.Principal.WindowsIdentity.GetCurrent();
                //var principal = new System.Security.Principal.WindowsPrincipal(identity);
                //if (!principal.IsInRole(System.Security.Principal.WindowsBuiltInRole.Administrator))
                //  throw new Exception($"Cannot delete task with your current identity '{identity.Name}' permissions level." +
                //"You likely need to run this application 'as administrator' even if you are using an administrator account.");

                // Remove the task we just created
                try
                {
                    ts.RootFolder.DeleteTask(taskName);
                }
                catch (Exception ex)
                { log.Error(ex); }
            }
        }

        public static void ExitOrNotifiy(string message)
        {
            log.Fatal("Exiting Application: {0}", message);
            if (ProgramOptionParser.DoAutostart())
            {
                Configuration.ShutDown(true);
            }
            else
            {
                try
                {
                    DialogResult d = MessageBox.Show("Can not continue. Click OK to Exit.", message, 
                        MessageBoxButtons.OKCancel,
                        MessageBoxIcon.Error);

                    if (d.Equals(DialogResult.OK))
                    { Configuration.ShutDown(true); }
                    else
                    { log.Fatal("Exit aborted by user."); }
                }
                catch (Exception ex)
                { log.Error(ex); }
            }
        }
    }
}
