﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Timers;
using System.Text;

namespace ReDirector
{
    using TradingTechnologies.TTAPI;
    using NLog;
    using System.Diagnostics;

    class Strategy : IDisposable
    {
        private static Logger log = LogManager.GetCurrentClassLogger();
        private System.Timers.Timer aTimer;
        private int data_count = 0;
        private int error_message_suppression = 0;

        private UniversalLoginTTAPI m_apiInstance = null;
        private WorkerDispatcher m_disp = null;

        private DataModel _hdm = null;
        private object m_lock = new object();
        private bool m_disposed = false;

        private List<TimeAndSalesSubscription> m_tnsSub = new List<TimeAndSalesSubscription>();
        private List<ProductLookupSubscription> m_prodLUSub = new List<ProductLookupSubscription>();
        private List<InstrumentCatalogSubscription> m_instCatSub = new List<InstrumentCatalogSubscription>();

        private int futures_count = 0;
        private int spread_count = 0;
        private int not_found = 0;

        public Strategy(UniversalLoginTTAPI api)
        {
            m_apiInstance = api;
            
            _hdm = new DataModel();
            // set timer event (for diagnostics) to fire after 1 seconds
            StartTimer(1*1000);
        }

        private void StartTimer(int interval)
        {
            aTimer = new System.Timers.Timer(interval)
            {
                AutoReset = true,
                Enabled = true
            };
            aTimer.Elapsed += OnTimedEvent;

        }

        int counter = 0;

        public bool IsDisposing { get; private set; }

        private void OnTimedEvent(object sender, ElapsedEventArgs e)
        {
            // update form once per second 
            if (data_count > 0)
            {
                Configuration.UpdateUIStatusline(String.Format("Inserts: {0}", data_count));
                if (DateTime.Now.Second == 0) log.Info("Inserts: {0}", data_count);
            }
            else
            {
                Configuration.UpdateUIStatusline(String.Format("Startup: {0}s", Math.Floor(GetRunTime().TotalSeconds)));
            }
            // show subscription progress until stabilized 
            if (!counter.Equals(m_tnsSub.Count))
            {
                counter = m_tnsSub.Count;
                log.Info("{0}s -> Price subscriptions: {1} (F:{5}/S:{6}) Products:{2} ProductLookups: {3} InstrumentCatalogLookups: {4}",
                      GetRunTime().TotalSeconds, m_tnsSub.Count, _hdm.ProductList.Count, m_prodLUSub.Count, m_instCatSub.Count,
                      this.futures_count, this.spread_count);
                // update contract count
                Configuration.UpdateCounts(this.futures_count, this.spread_count);
            }
            // if end time has been reached shutdown application 
            if (DateTime.Now >= Configuration.ShutDownTime)
            {
                log.Info("Executing shutdown at prescheduled time: {0}", Configuration.ShutDownTime);
                Configuration.ShutDown(true);
            }
        }

        private TimeSpan GetRunTime()
        {
            return DateTime.UtcNow - Process.GetCurrentProcess().StartTime.ToUniversalTime(); 
        }

        public void Start()
        {
            log.Trace("Managed Thread ID: {0} Name: {1}", Thread.CurrentThread.ManagedThreadId, Thread.CurrentThread.Name);
            // Attach a WorkerDispatcher to the current thread
            m_disp = Dispatcher.AttachWorkerDispatcher();
            // beginInvoke == asyncronous running
            m_disp.BeginInvoke(new Action(Init));
            
            m_disp.Run();
        }

        // launch product lookup using _hdm
        public void Init()
        {
            log.Trace("Managed Thread ID: {0} Name: {1}", Thread.CurrentThread.ManagedThreadId, Thread.CurrentThread.Name);
            foreach (ProductDetails pd in _hdm.ProductList)
            {
                LaunchProductLookup(pd, ProductType.Future);
                LaunchProductLookup(pd, ProductType.Spread);
            }
        }

        private void LaunchProductLookup(ProductDetails pd, ProductType pt)
        {
            ProductLookupSubscription pls = new ProductLookupSubscription(m_apiInstance.Session, Dispatcher.Current,
                new ProductKey(pd._marketKey, pt,  pd._product))
            {
                Tag = pd
            };
            pls.Update += ProductLookupHandler;
            m_prodLUSub.Add(pls);
            pls.Start();
        }

        
        // Event Handlers

        private void ProductLookupHandler(object sender, ProductLookupSubscriptionEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Product != null)
                {   // Product was found 
                    InstrumentCatalogSubscription ics = new InstrumentCatalogSubscription(e.Product, Dispatcher.Current);
                    ics.InstrumentsUpdated += InstrumentCatalog_Updated;
                    // Tag is ProductDetails class
                    ics.Tag = ((ProductLookupSubscription)sender).Tag;
                    m_instCatSub.Add(ics);
                    ics.Start();

                    if (e.IsFinal)
                    {
                        string message = String.Format("Getting contracts for: {0} {1} {2}", e.Product.Market, e.Product.Name, e.Product.Type);
                        // update form list on main Ui
                        Configuration.CrossThreadUILbUpdate(message);
                    }
                }
                else // e.Product == null
                {
                    if (e.IsFinal)
                    {
                        not_found++;
                        // Product was not found and TT API has given up looking for it 
                        log.Warn("Cannot find {0} product(s).", not_found);
                    }
                }
            }
            else // e.Error != null
            {
                if (e.Error.Message.Equals("Market not available."))
                {
                    error_message_suppression++;
                    if (error_message_suppression > m_prodLUSub.Count)
                    {
                        log.Debug("Product Lookup Error: {0}", e.Error.Message);
                    }
                }
            }
        }

        private void InstrumentCatalog_Updated(object sender, InstrumentCatalogUpdatedEventArgs e)
        {
            if (e.Added != null && e.Error == null)
            {
                // updated cast format
                // https://msdn.microsoft.com/en-us/library/cc221403(v=vs.95).aspx
                InstrumentCatalogSubscription ics = sender as InstrumentCatalogSubscription;
                ProductDetails pd = ics.Tag as ProductDetails;

                foreach (Instrument i in e.Added)
                {
                    if (i.Product.Type.Equals(ProductType.Spread))
                    {
                        // only create subscriptions for calendar spreads
                        if (!i.InstrumentDetails.Combination.Equals(Combination.Calendar))
                        { continue; }
                        this.spread_count++;
                    }
                    // set front month for relative contract calculation
                    if (i.Product.Type.Equals(ProductType.Future))
                    {
                        // set front month contract to earliest expiration (Product level Information)
                        if (i.InstrumentDetails.ExpirationDate.ToDateTime().Add(Configuration.ShutDownTime.TimeOfDay) > Configuration.StartupTime)
                        {
                            if (pd._front_month > i.InstrumentDetails.ExpirationDate.ToDateTime())
                            {
                                pd._front_month = i.InstrumentDetails.ExpirationDate.ToDateTime();
                            }

                            int cid = _hdm.GetCreate_ContractID(pd._ticker_id, i.GetFormattedName(InstrumentNameFormat.Expiry), i.InstrumentDetails.ExpirationDate.ToDateTime());
                            // Contract ID is contract level detail
                            _hdm.ContractIDList.Add(i.Key.SeriesKey, cid);
                            this.futures_count++;
                            // DEBUG Verify: if (i.InstrumentDetails.Name.Contains("NG")) log.Debug("{0}: {1}", i.InstrumentDetails.Name, i.InstrumentDetails.ExpirationDate);
                        }
                    }
                    // Start a Time & Sales subscription
                    TimeAndSalesSubscription tsSub = new TimeAndSalesSubscription(i, Dispatcher.Current);
                    // set up futures event handler ( can be setup inside if statement )
                    tsSub.Update += TimeSales_Update;
                    tsSub.Tag = pd; 
                    m_tnsSub.Add(tsSub);
                    tsSub.Start();
                }
            }
            if (e.Error != null) { log.Debug("InstrumentCatalog_Updated Error: {0}", e.Error.Message); }
            if (e.Added == null) { log.Debug("Added is null"); }
        }
        
        private void TimeSales_Update(object sender, TimeAndSalesEventArgs e)
        {
            // process the update
            if (e.Error == null && e.Data.Count > 0)
            {
                this.data_count += e.Data.Count;

                ProductDetails pd = (ProductDetails)((TimeAndSalesSubscription)sender).Tag;
                if (e.Instrument.Product.Type.Equals(ProductType.Future))
                {
                    _hdm.InsertFuturesData(e.Data, e.Instrument, pd);
                }
                else if (e.Instrument.Product.Type.Equals(ProductType.Spread))
                {
                    _hdm.InsertSpreadData(e.Data, e.Instrument, pd);
                }
            }
            
            if (e.Error != null)
            {
                log.Error(e.Error.Message);
                log.Error(e.Error);
            }
        }

        // Dispose of resources on shutdown
        public void Dispose()
        {
            // TODO: not fully implemented - use this to prevent actions that cause crash on start with immediate stop
            IsDisposing = true;
            // TTAPI docs show setting objects to null.
            // https://online-help.tradingtechnologies.com/TT_API-7.17.10/webframe.html#topic84.html
            lock (m_lock)
            {
                log.Debug("Passed Lock");
                if (!m_disposed)
                {
                    // stop and dispose timer event
                    aTimer.Stop();
                    aTimer.Elapsed -= OnTimedEvent;
                    if (aTimer != null) aTimer.Dispose();
                    log.Debug("Timer disposed");
                    // Unattached callbacks and dispose of all subscriptions
                    m_prodLUSub.ForEach(sub => {
                        if (sub != null)
                        {
                            log.Debug("Product Subscription Dispose: {0}", ((ProductDetails)sub.Tag)._product);
                            sub.Tag = null;
                            sub.Update -= ProductLookupHandler;
                            sub.Dispose();
                        }
                    });
                    m_prodLUSub.Clear();
                    log.Debug("disposed of ProductLookupSubscriptions");
                    foreach (InstrumentCatalogSubscription sub in m_instCatSub)
                    {
                        if (sub != null)
                        {
                            sub.Tag = null;
                            sub.InstrumentsUpdated -= InstrumentCatalog_Updated;
                            sub.Dispose();
                        }
                    }
                    m_instCatSub.Clear();
                    log.Debug("disposed of InstrumentCatalogSubscription");
                    int counter = 0;
                    // TODO: exception thrown if shutdown during startup
                    foreach (TimeAndSalesSubscription sub in m_tnsSub)
                    {
                        if (sub != null)
                        {
                            sub.Tag = null;
                            sub.Update -= TimeSales_Update;
                            sub.Dispose();
                        }
                        if (++counter % 1000 == 0) log.Debug("disposed of {0} tns subs", counter);
                    }
                    m_tnsSub.Clear();
                    log.Debug("disposed  of TimeAndSalesSubscriptions");
                    log.Debug("Time&Sales subscriptions:{0} Product Details:{1} ProductLookups: {2} InstrumentCatalogLookups: {3}",
                                     m_tnsSub.Count, _hdm.ProductList.Count, m_prodLUSub.Count, m_instCatSub.Count);
                    _hdm.CloseDB();
                    _hdm.Dispose();

                    // Shutdown the Dispatcher
                    if (m_disp != null)
                    {
                        m_disp.BeginInvokeShutdown();
                        m_disp = null;
                    }

                    m_disposed = true;
                    log.Debug("{0} is disposed", System.Reflection.MethodInfo.GetCurrentMethod().DeclaringType.Name);
                }
            }
        }
    }
}
