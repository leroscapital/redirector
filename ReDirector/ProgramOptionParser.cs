﻿using System;
using System.Collections.Generic;

namespace ReDirector
{
    using NLog;

    internal static class ProgramOptionParser
    {
        static Logger log = LogManager.GetCurrentClassLogger();

        static Dictionary<string, string> arguments = new Dictionary<string, string>();

        // constructor
        static ProgramOptionParser()
        { }

        internal static void PopulateArgDict(string[] a)
        {
            char[] delimiter = { '-', '=' };
            foreach (string s in a)
            {
                // left over from --ttpw --dbpw on command line  can still work for ttpw, dbpw, dbro
                string[] p = s.Split(delimiter, StringSplitOptions.RemoveEmptyEntries);
                if (p.Length == 2)
                {
                    arguments.Add(p[0].ToUpper(), p[1]);
                }
                else if (p.Length == 1)
                {
                    arguments.Add(p[0].ToUpper(), "ENABLED");
                }
                else
                {
                    log.Error("Unrecognized command line argument: [{0}]", String.Join(", ", p));
                }
            }
            log.Debug("# of command line arguments found: {0}", arguments.Count);
        }

        internal static bool DoAutostart()
        {
            if (arguments.ContainsKey("AUTO")) return true;
            return false;
        }
    }
}
