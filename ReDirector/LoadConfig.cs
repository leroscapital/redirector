﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;

namespace TTAPI2MYSQL
{
    using NLog;

    class LoadConfig
    {
        private static Logger log = LogManager.GetCurrentClassLogger();
        public Dictionary<string, string> settings = new Dictionary<string, string>();

        //Constructor
        public LoadConfig(string filename)
        {
            if (File.Exists(filename))
            {
                readConfigFileIntoDictionary(filename);
            }
            else
            {
                log.Info(filename + " config file is not found!");
                MessageBox.Show("Configuration file missing:\n" + filename + " not found.", "Error: Application will Exit",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                Environment.Exit(1);
            }
        }

        private void readConfigFileIntoDictionary(string fn)
        {
            string line;
            char[] delimiter = { ':', ' ' };
            StreamReader configFile = new StreamReader(fn);

            int lineCount = 0;
            while ((line = configFile.ReadLine()) != null)
            {
                lineCount++;
                string[] p = line.Split(delimiter, StringSplitOptions.RemoveEmptyEntries);
                if (p.Length == 2)
                {
                    settings.Add(p[0].ToUpper(), p[1]);
                }
                else if (p.Length == 0)
                {
                    // skip blank lines
                }
                else
                {
                    log.Error("Bad line format in config file {0} on line {1}", fn, lineCount);
                    log.Info("Hit any key to exit");
                    Console.Read();
                    Environment.Exit(1);
                }
       
            }
            configFile.Close();

        }
    }
}
