﻿using System;
namespace ReDirector
{
    // struct for encapsulating product details  **!! struct is immutable changed to class solved all problems
    public class ProductDetails
    {
        public string _marketKey;
        public string _product;
        public int _uid;        // underlying id
        public int _ticker_id;
        public double _tick_currency_value;
        public int _contract_size;
        public string _name;    // table name

        // types of product to load
        // public Boolean _LoadFutures = true;
        // public Boolean _LoadSpreads = true;

        // set at construction to be reset when actual data is received
        public DateTime _front_month; 

        // set after construction
        public DateTime _expiration;
        public string _tableName;
        public string _spreadTableName;
        public string _futuresTableName;

        // there is only one product details object per product -- this class can't hold these contract level details
//        public int _contract_id;
//        public int _relative;
        
        //Contstructor
        public ProductDetails(string mk, string prod, int uid, int ticker_id, double tick_currency_value, int contract_size, string name)
        {
            _marketKey = mk.Trim();
            _product = prod.Trim();
            _uid = uid;
            _ticker_id = ticker_id;
            _name = name.Trim();
            _tick_currency_value = tick_currency_value;
            _contract_size = contract_size;

            _front_month = new DateTime(9999, 1, 1);
        }

    }
}
