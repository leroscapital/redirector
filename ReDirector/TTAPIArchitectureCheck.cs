﻿using System;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ReDirector
{
    using NLog;
    /// <summary>
    /// Utility to confirm that the compiler settings are compatible with the version of TT API installed
    /// </summary>
    class TTAPIArchitectureCheck
    {
        private static Logger log = LogManager.GetCurrentClassLogger();
        public static string ErrorString { get; private set; }
        /// <summary>
        /// Verify the application build settings match the architecture of the TT API installed
        /// </summary>
        public static bool Validate(bool produce_error = false)
        {
            try
            {
                System.Diagnostics.FileVersionInfo fileVersionInfo = 
                    System.Diagnostics.FileVersionInfo.GetVersionInfo((new System.IO.FileInfo("TradingTechnologies.TTAPI.dll")).FullName);

                System.Reflection.Assembly appAssembly = System.Reflection.Assembly.GetExecutingAssembly();
                System.Reflection.Assembly apiAssembly = System.Reflection.Assembly.ReflectionOnlyLoadFrom(fileVersionInfo.FileName);

                System.Reflection.PortableExecutableKinds appKinds, apiKinds;
                System.Reflection.ImageFileMachine appImgFileMachine, apiImgFileMachine;

                appAssembly.ManifestModule.GetPEKind(out appKinds, out appImgFileMachine);
                apiAssembly.ManifestModule.GetPEKind(out apiKinds, out apiImgFileMachine);
                log.Info("TTAPI: {0} {1}", fileVersionInfo.FileVersion.Replace(',','.'), apiKinds);

                if (produce_error)
                {
                    if (!appKinds.HasFlag(apiKinds))
                    {
                        ErrorString = String.Format("WARNING: This application must be compiled as a {0} application to run with a {0} version of TT API.",
                                      (apiKinds.HasFlag(System.Reflection.PortableExecutableKinds.Required32Bit) ? "32Bit" : "64bit"));
                        log.Debug(ErrorString);
                        log.Debug("Application: {0} API: {1}", appKinds, apiKinds);
                        return false;
                    }
                    else
                    {
                        ErrorString = "";
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorString = String.Format("ERROR: An error occured while attempting to verify the application build settings match the architecture of the TT API installed. {0}", ex.Message);
                log.Error(ex);
                return false;
            }

            return true;
        }

        public static void test()
        {
            if (Validate())
            {
                log.Info("Architecture check passed");
            }
            else
            {
                log.Fatal("Architecture check failed");
                log.Fatal("{0}", ErrorString);
                Utility.ExitOrNotifiy("ERROR: Incompatible TTAPI Libraries.");
            }
        }
    }
}
