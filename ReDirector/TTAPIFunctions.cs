﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows.Forms;

namespace ReDirector
{
    using NLog;
    using System.Diagnostics;
    using TradingTechnologies.TTAPI;

    // Main TT API class
    class TTAPIFunctions : IDisposable
    { 
        private static Logger log = LogManager.GetCurrentClassLogger();    

        private UniversalLoginTTAPI m_apiInstance = null;
        private WorkerDispatcher m_disp = null;
        private object m_lock = new object();
        internal bool m_disposed = false;
        internal bool _TTAPI_ShutdownCompleted = false;

        private List<Strategy> m_strats = new List<Strategy>();

        // Primary constructor
        public TTAPIFunctions()
        {
            log.Trace("Managed Thread ID: {0} Name: {1}", Thread.CurrentThread.ManagedThreadId, Thread.CurrentThread.Name);
        }

        // Create and start the Dispatcher
        public void Start()
        {
            log.Trace("Managed Thread ID: {0} Name: {1}", Thread.CurrentThread.ManagedThreadId, Thread.CurrentThread.Name);
            // Attach a WorkerDispatcher to the current thread
            
            m_disp = Dispatcher.AttachWorkerDispatcher();
            m_disp.BeginInvoke(new Action(Init));
            m_disp.Run();
        }

        // Initialize TT API
        public void Init()
        {
            // Use "Universal Login" Login Mode
            ApiInitializeHandler h = new ApiInitializeHandler(TTApiInitComplete);
            // https://online-help.tradingtechnologies.com/TT_API-7.17.10/webframe.html#topic2986.html
            UniversalLoginTTAPIOptions ulOptions = new UniversalLoginTTAPIOptions()
            {
                EnableImplieds = true,       //default true
                StartOrderFillFeed = false,  // default true
                //UseExchangeNames = true    //default false
                //UseProductAlias default false : use Guardian product alias
                //EnablePriceSubscriptionConsolidation = true,  I did not see any memory use benefit with this.
            };
            Configuration.CrossThreadUILbUpdate("Logging into TTAPI...");
            string user = Configuration.GetTTUser();
            string pass = Configuration.GetTTPassword();
            TTAPI.CreateUniversalLoginTTAPI(Dispatcher.Current, user, pass, ulOptions, h);
        }

        // Event notification for status of TT API initialization
        public void TTApiInitComplete(TTAPI api, ApiCreationException ex)
        {
            if (ex == null)
            {
                log.Info("TTAPI Initialized");
                // Authenticate your credentials
                m_apiInstance = (UniversalLoginTTAPI)api;
                m_apiInstance.AuthenticationStatusUpdate += TTAPI_AuthenticationStatusUpdate;
                m_apiInstance.LicenseIssue += TTAPI_apiInstance_LicenseIssue;
                m_apiInstance.Session.AdminMessage += Session_AdminMessage;
                m_apiInstance.Start();
            }
            else
            {
                log.Fatal("TT API Initialization Failed: {0}", ex.Message);
                Configuration.CrossThreadUILbUpdate("ERROR: TTAPI Initialization failed!");
            }
        }

        private void Session_AdminMessage(object sender, AdminMessageEventArgs e)
        {
            log.Info("Admin Message: {0}", e.Message);
        }

        // log errors if there is a license problem
        private void TTAPI_apiInstance_LicenseIssue(object sender, LicenseIssueEventArgs e)
        {
            log.Error("License Issue Event Triggered");
            log.Error("{0}; ({1} {2})", e.Message, e.FeedType, e.GatewayKey);

            // https://docs.microsoft.com/en-us/dotnet/csharp/pattern-matching
            if (e is GatewayLicenseExpiringEventArgs gle)
            {
                // The license for a TT Gateway to which it is attempting to connect is either expired
                // or will be expiring within 10 days.
                log.Error("Gateway : {0}, Feed Type : {1}, IP Address : {2}, License Expiration Date : {3}, Message : {4}", gle.GatewayKey, gle.FeedType, gle.IPAddress, gle.ExpiryDate, gle.Message);
            }
            else if (e is LicenseExpiringEventArgs le)
            {
                // The X_TRADER Pro license that the TT API has acquired will be expiring within 10 days.
                log.Error("Gateway : {0}, Feed Type : {1}, License Expiration Date : {2}, Message : {3}", le.GatewayKey, le.FeedType, le.ExpiryDate, le.Message);
            }
            else if (e is NoLicenseEventArgs nle)
            {
                // The TT API cannot acquire an X_TRADER Pro license.
                log.Error("Gateway : {0}, Feed Type : {1}, Message : {2}", nle.GatewayKey, nle.FeedType, nle.Message);
            }
        }

        // main start point for further startup  Event notification for status of authentication
        public void TTAPI_AuthenticationStatusUpdate(object sender, AuthenticationStatusUpdateEventArgs e)
        {
            if (e.Status.IsSuccess)
            {
                m_apiInstance.Session.MarketCatalog.FeedStatusChanged += MarketCatalog_FeedStatusChanged;
                Configuration.CrossThreadUILbUpdate("TTAPI Universal Login: Successful.");

                // Start Time & Sales subscriptions on a separate thread
                Stopwatch sw = Stopwatch.StartNew();
                Strategy s1 = new Strategy(m_apiInstance);
                sw.Stop();
                log.Debug("new Strategy creation time(ms): {0}", sw.Elapsed.TotalMilliseconds);
                // keep a list of references to the strategy to have access throughout this class
                m_strats.Add(s1);
                // C# users can omit the ThreadStart or ParameterizedThreadStart delegate constructor when creating a thread
                // https://docs.microsoft.com/en-us/dotnet/api/system.threading.threadstart?view=netframework-4.7
                Thread workerThread1 = new Thread(s1.Start)
                {
                    Name = "FUTURES"
                };
                workerThread1.Start();

            }
            else
            {
                // update listbox in main GUI
                Configuration.CrossThreadUILbUpdate("TTAPI Universal Login: FAILED!");
                log.Fatal(e.Status.StatusMessage);
            }
        }

        private void MarketCatalog_FeedStatusChanged(object sender, FeedStatusChangedEventArgs e)
        {
            log.Debug(sender.GetType());
            if (e.Feed != null && e.Feed.Status != null)
            {
                log.Info("Gateway: {0}[{1}] Status: {2}", e.Feed.Market, e.Feed.Name, e.Feed.Status.Availability);
            }
        }

        // Shuts down the TT API
        public void Dispose()
        {
            log.Trace("Managed Thread ID: {0} Name: {1}", Thread.CurrentThread.ManagedThreadId, Thread.CurrentThread.Name);
            lock (m_lock)
            {
                log.Debug("Passed lock.");
                if (!m_disposed)
                {
                    foreach (Strategy s  in m_strats)
                    {
                        s.Dispose();
                    }
                    log.Debug("Finished dispose of Strategies from within TTAPIFunctions.");
                    // Begin shutdown the TT API
                    TTAPI.ShutdownCompleted += TTAPI_ShutdownCompleted;
                    TTAPI.Shutdown();
                    m_disposed = true;
                }
            }
        }

        // Event notification for completion of TT API shutdown
        public void TTAPI_ShutdownCompleted(object sender, EventArgs e)
        {
            log.Debug("TTAPI shutdown completed.");
            // Shutdown the Dispatcher
            if (m_disp != null)
            {
                m_disp.BeginInvokeShutdown();
                m_disp = null;
            }

            // Dispose of any other objects / resources
            log.Info("Program finished!");
            _TTAPI_ShutdownCompleted = true;
        }
    }
}
