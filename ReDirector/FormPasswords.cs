﻿using System;
using System.Windows.Forms;

namespace ReDirector
{
    using NLog;

    public partial class FormPasswords : Form
    {
        /// <summary>
        /// This form is only launched when all settings are complete EXCEPT for 1 or more passwords
        /// </summary>
        private static Logger log = LogManager.GetCurrentClassLogger();
        private bool database_password_entered = false;
        private bool tt_password_entered = false;
        private bool ro_database_password_entered = false;

        public FormPasswords()
        {
            InitializeComponent();
            SetupForm();
        }

        private void SetupForm()
        {
            this.ActiveControl = this.password_dbrw;
            Configuration.AddDataBinding(password_dbrw, "Passwords.RWDB");
            Configuration.AddDataBinding(password_dbro, "Passwords.RODB");
            Configuration.AddDataBinding(password_tt, "Passwords.TTPW");
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            
            this.Close();
        }

        private void DB_Password_TextChanged(object sender, EventArgs e)
        {
            Validate((TextBox)sender, ref database_password_entered);
        }

        private void TT_Password_TextChanged(object sender, EventArgs e)
        {
            Validate((TextBox)sender, ref tt_password_entered);
        }

        private void Password_dbro_TextChanged(object sender, EventArgs e)
        {
            Validate((TextBox)sender, ref ro_database_password_entered);
        }

        private void Validate(TextBox t, ref bool password)
        {
            if (t.TextLength > 0)
            {
                password = true;
            }
            else
            {
                password = false;
            }
            TestButton();
        }

        private void TestButton()
        {
            if (database_password_entered && ro_database_password_entered && tt_password_entered)
            {
                button1.Enabled = true;
            }
            else
            {
                button1.Enabled = false;
            }
        }

        private void FormPasswords_FormClosing(object sender, FormClosingEventArgs e)
        {
            log.Trace(".");

            if (this.checkBox1.Checked)
            {
                Configuration.WriteConfig();
                Configuration.frmMain.UpdateMainUIMessage("Passwords saved.");
                log.Info("Passwords saved to file.");
                
            }

            if (database_password_entered && ro_database_password_entered && tt_password_entered)
            {
                Configuration.frmMain.Enable_Startup(true);
                log.Info("All passwords configured.");
            }
            else
            {
                Configuration.frmMain.Enable_Startup(false);
                Configuration.frmMain.UpdateMainUIMessage("All passwords are not configured.");
                log.Info("All passwords are not configured.");
                Configuration.frmMain.UpdateMainUIMessage("Click 'Edit Settings' button  to enter passwords.");
            }
        }

        private void FormPasswords_Load(object sender, EventArgs e)
        {
        }
    }
}
